Welcome to Asset Aliases module

**Description:

This module provides support functions for asset modules to
automatically generate aliases based on appropriate criteria.
It has three dependencies to Asset, Pathauto and Entity tokens modules.


**Installation:

Asset Aliases is an extension to the asset module, which must be downloaded
and enabled.

Asset Aliases also relies on the Pathauto and Entity tokens modules, which must
be downloaded and enabled separately.

1. Unpack the Asset Aliases folder and contents in the appropriate modules
directory of your Drupal installation.  This is probably
  sites/all/modules/
2. Enable the Asset Aliases module in the administration tools.
3. If you're not using Drupal's default administrative account, make
sure "administer pathauto" is enabled through access control administration.


**Administration

This module implement the Pathauto administration.
